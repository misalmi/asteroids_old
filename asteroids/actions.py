import utils
import sprites as s

import random
import multiprocessing.queues

KEYS = ("accelerate", "drag", "rotate_left", "rotate_right", "shoot")
        
class KeyActions(object):
    def __init__(self, player_ship, key_conf, key_states):
        self.ship = player_ship
        self._key_conf = key_conf
        self._key_states = key_states
        self._last_shot = 0
        
    def _check_keys(self, action):
        return any(self._key_states[key] for key in self._key_conf[action])

    def update(self, dt, ships, asteroids, images, batch, win_size):
        if not self.ship.alive:
            if self._check_keys("respawn"):
                sprites = [ship.ship_sprite for ship in ships] + asteroids 
                pos = utils.search_pos(first_pos=s.Ship.default_pos, sprites=sprites, 
                                       win_size=win_size)
                self.ship = s.create_ship(self.ship.name_label.text, images, pos=pos, batch=batch)
                return self.ship
            else:
                return

        if self._check_keys("accelerate"):
            self.ship.accelerate(dt)
        else:
            self.ship.stop_acceleration()
        if self._check_keys("drag"):
            self.ship.drag(dt)
        if self._check_keys("rotate_left"):
            self.ship.rotate_left(dt)
        if self._check_keys("rotate_right"):
            self.ship.rotate_right(dt)
        if self._check_keys("shoot"):
            if self._last_shot > 10:
                self.ship.shoot_missile(dt, images.missile_img, images.explosion_img)
                self._last_shot = 0
        self._last_shot += 1

class AIActions(object):
    def __init__(self, reader_queue, writer_queue):
        self._reader_queue = reader_queue
        self._writer_queue = writer_queue
        self._ships = {}
        self._last_shots = {}

    def _read_remote(self, num_messages):
        messages = []
        for i in range(num_messages):
            try:
                messages.append(self._reader_queue.get_nowait())
            except multiprocessing.queues.Empty:
                return messages
        return messages

    def _create_ship(self, ship_id, name, sprites, images, batch, win_size):
        pos = utils.search_pos(first_pos=s.Ship.default_pos, sprites=sprites, 
                               win_size=win_size)
        v = (0, 0)
        ship = s.create_ship(name=name, images=images, pos=pos, batch=batch, velocity=v)
        self._ships[ship_id] = ship
        self._last_shots[ship_id] = 0
        return ship

    def _update_ship(self, dt, ship_id, cmd, images):
        ship = self._ships[ship_id]
        if not ship.alive:
            return

        if cmd == "accelerate":
            ship.accelerate(dt)
        elif cmd == "drag":
            ship.drag(dt)
        elif cmd == "rotate_left":
            ship.rotate_left(dt)
        elif cmd == "rotate_right":
            ship.rotate_right(dt)
        elif cmd == "shoot":
            if self._last_shots[ship_id] > 10:
                ship.shoot_missile(dt, images.missile_img, images.explosion_img)
                self._last_shots[ship_id] = 0

    def _send_data(self, ships, asteroids):
        ship_data = []
        missile_data = []
        sprite_data_str = "{x},{y},{vx},{vy}"
        for ship in ships:
            sprite = ship.ship_sprite
            missile = ship.missile
            if ship.alive:
                ship_data.append(sprite_data_str.format(x=sprite.x, y=sprite.y, 
                                                        vx=sprite.velocity_x, vy=sprite.velocity_y))
            if missile.is_shot():
                missile_data.append(sprite_data_str.format(x=missile.x, y=missile.y, 
                                                           vx=missile.velocity_x, vy=missile.velocity_y))
        ship_data = " ".join(ship_data)
        missile_data = " ".join(missile_data)
        asteroid_data = " ".join(sprite_data_str.format(x=asteroid.x, y=asteroid.y, 
                                                        vx=asteroid.velocity_x, vy=asteroid.velocity_y) 
                                 for asteroid in asteroids)
        for ship_id in self._ships:
            self._last_shots[ship_id] += 1
            ship = self._ships[ship_id]
            sprite = ship.ship_sprite
            addr = str(ship_id[0])
            player_data_str = "{alive},{x},{y},{vx},{vy},{rot},{rot_speed},{shot}"
            player_data = player_data_str.format(alive=ship.alive, x=sprite.x, y=sprite.y,
                                                 vx=sprite.velocity_x, vy=sprite.velocity_y,
                                                 rot=sprite.rotation, rot_speed=sprite.rotate_speed, 
                                                 shot=ship.missile.is_shot())

            self._writer_queue.put(("ships " + ship_data, addr))
            self._writer_queue.put(("asteroids " + asteroid_data, addr))
            self._writer_queue.put(("missiles " + missile_data, addr))
            self._writer_queue.put(("player " + player_data, addr))

    def update(self, dt, ships, asteroids, images, batch, win_size):
        NUM_MESSAGES = 20
        messages = self._read_remote(NUM_MESSAGES)
        new_ships = []
        for msg in messages:
            ship_id, cmd = msg
            if cmd.startswith("create"):
                if ship_id in self._ships and self._ships[ship_id].alive:
                    continue
                name = cmd.split()[-1]
                sprites = [ship.ship_sprite for ship in ships] + asteroids
                ship = self._create_ship(ship_id, name, sprites, images, batch, win_size)
                new_ships.append(ship)
                ships.append(ship)
            elif ship_id in self._ships:
                self._update_ship(dt, ship_id, cmd, images)
        self._send_data(ships, asteroids)
        return new_ships
                

