import argparse
import time
import random
import pickle

import pyglet

import utils
import monitor
import sprites as s
import actions
import udp_conns
import local_ai

FRAMERATE = 60.0
SAVEFILE = "savefile.dat"

KEY_CONF_SINGLE_PLAYER = {
    "accelerate" : (pyglet.window.key.UP, pyglet.window.key.W,),
    "drag" :  (pyglet.window.key.DOWN, pyglet.window.key.S,),
    "rotate_left" : (pyglet.window.key.LEFT, pyglet.window.key.D,),
    "rotate_right" : (pyglet.window.key.RIGHT, pyglet.window.key.A,),
    "shoot" : (pyglet.window.key.SPACE,),
    "respawn" : (pyglet.window.key.R,),
}

KEY_CONF_HOTSEAT_PLAYER1 = {
    "accelerate" : (pyglet.window.key.UP,),
    "drag" :  (pyglet.window.key.DOWN,),
    "rotate_left" : (pyglet.window.key.LEFT,),
    "rotate_right" : (pyglet.window.key.RIGHT,),
    "shoot" : (pyglet.window.key.SPACE,),
    "respawn" : (pyglet.window.key.PLUS,),
}

KEY_CONF_HOTSEAT_PLAYER2 = {
    "accelerate" : (pyglet.window.key.W,),
    "drag" :  (pyglet.window.key.S,),
    "rotate_left" : (pyglet.window.key.A,),
    "rotate_right" : (pyglet.window.key.D,),
    "shoot" : (pyglet.window.key.LCTRL,),
    "respawn" : (pyglet.window.key.R,),
}

def create_player_actions(name, images, other_sprites, batch, win_size, key_conf, key_state_handler):
    pos = utils.search_pos(first_pos=s.Ship.default_pos, sprites=other_sprites, win_size=win_size)
    ship = s.create_ship(name, images, pos=pos, batch=batch)
    return actions.KeyActions(ship, key_conf, key_state_handler)

class GameWindow(pyglet.window.Window):
    """Main window of the Asteroids game."""
    ASTEROID_DEFAULT_V_FACTOR = 100.0, 100.0

    def __init__(self, width, height, num_asteroids, human_player=True, hotseat=True, ai_actions=None):
        super(GameWindow, self).__init__(width, height)
        self.win_size = width, height
        self.main_batch = pyglet.graphics.Batch()

        bg_img = pyglet.resource.image("stars.png")
        ship_img = utils.center_image(pyglet.resource.image("ship.png"))
        flame_img = pyglet.resource.image("flame.png")
        flame_img.anchor_x = flame_img.width - 210
        flame_img.anchor_y = flame_img.height - 600
        missile_img = utils.center_image(pyglet.resource.image("missile.png"))
        asteroid_img = utils.center_image(pyglet.resource.image("asteroid.png"))
        explosion_img = utils.center_image(pyglet.resource.image("explosion.png"))
        self.images = utils.Images(ship_img, flame_img, missile_img, asteroid_img, explosion_img)

        self.bg = pyglet.sprite.Sprite(img=bg_img, x=0, y=0, batch=self.main_batch,
                                       group=pyglet.graphics.OrderedGroup(-1))

        self.num_asteroids = num_asteroids
        self.current_asteroids = num_asteroids
        self.asteroids = s.create_asteroids_randomly(n=num_asteroids, img=asteroid_img, scale=1.0,
                                                     v_factor=self.ASTEROID_DEFAULT_V_FACTOR,
                                                     batch=self.main_batch, win_size=self.win_size)
        self.key_actions1 = None
        self.key_actions2 = None
        self.scoreboard1 = None
        self.scoreboard2 = None
        self.hiscoreboard = None
        self.ai_actions = ai_actions
        self.ships = []
        if human_player:
            key_state_handler = pyglet.window.key.KeyStateHandler()
            self.push_handlers(key_state_handler)
            if hotseat:
                 self.key_actions1 = create_player_actions("player 1", self.images, self.asteroids, self.main_batch,
                                                           self.win_size, KEY_CONF_HOTSEAT_PLAYER1, key_state_handler)
                 self.key_actions2 = create_player_actions("player 2", self.images, self.asteroids + [self.key_actions1.ship.ship_sprite],
                                                           self.main_batch, self.win_size, KEY_CONF_HOTSEAT_PLAYER2, key_state_handler)
                 self.ships.extend([self.key_actions1.ship, self.key_actions2.ship])
                 self.scoreboard1 = pyglet.text.Label("P1 score: ", font_size=20, x=20, y=height - 50,
                                                      batch=self.main_batch, group=pyglet.graphics.OrderedGroup(3))
                 self.scoreboard2 = pyglet.text.Label("P2 score: ", font_size=20, x=250, y=height - 50,
                                                      batch=self.main_batch, group=pyglet.graphics.OrderedGroup(3))
            else:
                self.key_actions1 = create_player_actions("player", self.images, self.asteroids, self.main_batch,
                                                          self.win_size, KEY_CONF_SINGLE_PLAYER, key_state_handler)
                self.ships.append(self.key_actions1.ship)
                self.scoreboard1 = pyglet.text.Label("Score: ", font_size=20, x=20, y=height - 50,
                                                      batch=self.main_batch, group=pyglet.graphics.OrderedGroup(3))

            self.hiscore = utils.load_hiscore(SAVEFILE)
            self.hiscoreboard = pyglet.text.Label("High score: " + str(self.hiscore), font_size=20, x=width - 20, y=height - 50,
                                                  anchor_x="right", batch=self.main_batch, group=pyglet.graphics.OrderedGroup(3))

    def on_draw(self):
        self.clear()
        self.main_batch.draw()

    def on_close(self):
        hiscore = utils.load_hiscore(SAVEFILE)
        if self.hiscore > hiscore:
            with open(SAVEFILE, 'wb') as f:
                pickle.dump([self.hiscore], f, protocol=2)

    def _check_asteroid_collision(self, ship, asteroid):
        MIN_ASTEROID_SCALE = 1.0
        if ship.alive and ship.collided_with(asteroid):
            ship.destroy(self.images.explosion_img)
        if ship.missile.is_shot() and ship.missile.collided_with(asteroid):
            ship.hide_missile()
            ship.score += 1
            asteroid.explode(self.images.explosion_img)
            self.asteroids.remove(asteroid)
            if asteroid.scale >= MIN_ASTEROID_SCALE:
                v_factor_x, v_factor_y = self.ASTEROID_DEFAULT_V_FACTOR
                v_factor = (v_factor_x / (asteroid.scale / 2), v_factor_y / (asteroid.scale / 2))
                new_asteroids = s.create_asteroids(n=4, img=self.images.asteroid_img, scale=asteroid.scale / 2,
                                                   v_factor=v_factor, batch=self.main_batch, pos=asteroid.position)
                self.asteroids.extend(new_asteroids)

    def _check_other_ship_collision(self, ship, other_ship):
        if other_ship.alive and ship.missile.is_shot() and ship.missile.collided_with(other_ship.ship_sprite):
            other_ship.destroy(self.images.explosion_img)
            ship.hide_missile()
            if ship != other_ship:
                ship.score += 1
        if ship != other_ship and ship.alive and other_ship.alive and ship.collided_with(other_ship.ship_sprite):
            ship.destroy(self.images.explosion_img)
            other_ship.destroy(self.images.explosion_img)

    def _check_collisions(self):
        for ship in self.ships[:]:
            for asteroid in self.asteroids[:]:
                self._check_asteroid_collision(ship, asteroid)
            for other_ship in self.ships[:]:
                self._check_other_ship_collision(ship, other_ship)

    def _spawn_new_asteroids(self):
        MAX_NUM_ASTEROIDS = 10
        hotseat = self.key_actions2 is not None

        asteroids_n = self.current_asteroids
        if ((not self.key_actions1.ship.alive and not hotseat) or
            (not self.key_actions1.ship.alive and not self.key_actions2.ship.alive)):
            self.current_asteroids = self.num_asteroids
            asteroids_n = self.num_asteroids
        elif self.current_asteroids < MAX_NUM_ASTEROIDS:
            self.current_asteroids += 1
            asteroids_n = self.current_asteroids
        for n in range(asteroids_n):
            sprites = []
            for ship in self.ships:
                sprites.extend([ship.ship_sprite, ship.flame, ship.missile])
            width, height = self.win_size
            pos = utils.search_pos(first_pos=(random.randint(0, width), random.randint(0, height)), sprites=sprites,
                                   win_size=self.win_size)
            self.asteroids.append(s.create_asteroid(img=self.images.asteroid_img, scale=1.0,
                                                    v_factor=self.ASTEROID_DEFAULT_V_FACTOR,
                                                    batch=self.main_batch, pos=pos))

    def _update_scoreboards(self, hotseat=False):
        player1_ship = self.key_actions1.ship
        if hotseat:
            player2_ship = self.key_actions2.ship
            if player1_ship.alive:
                self.scoreboard1.text = "P1 score " + str(player1_ship.score)
            else:
                self.scoreboard1.text = "P1 score: 0"
            if player2_ship.alive:
                self.scoreboard2.text = "P2 score: " + str(player2_ship.score)
            else:
                self.scoreboard2.text = "P2 score: 0"
            if self.hiscore < player2_ship.score:
                self.hiscoreboard.text = "High score: " + str(player2_ship.score)
                self.hiscore = player2_ship.score
        else:
            if player1_ship.alive:
                self.scoreboard1.text = "Score: " + str(player1_ship.score)
            if not hotseat:
                self.scoreboard1.text = "Score: 0"
        if self.hiscore < player1_ship.score:
            self.hiscoreboard.text = "High score: " + str(player1_ship.score)
            self.hiscore = player1_ship.score

    def update(self, dt):
        if self.key_actions1 is not None:
            new_ship = self.key_actions1.update(dt, self.ships[:], self.asteroids[:], self.images, self.main_batch,
                                                self.win_size)
            if new_ship is not None:
                self.ships.append(new_ship)
            if self.key_actions2 is not None:
                new_ship = self.key_actions2.update(dt, self.ships[:], self.asteroids[:], self.images, self.main_batch,
                                                    self.win_size)
                if new_ship is not None:
                    self.ships.append(new_ship)
                self._update_scoreboards(hotseat=True)
            else:
                self._update_scoreboards(hotseat=False)
        if self.ai_actions is not None:
            new_ships = self.ai_actions.update(dt, self.ships[:], self.asteroids[:], self.images, self.main_batch,
                                                   self.win_size)
            self.ships.extend(new_ships)
        for sprite in self.asteroids:
            sprite.update(dt, self.win_size)
        for ship in self.ships[:]:
            if ship.missile.alive:
                ship.missile.update(dt, self.win_size)
            if ship.alive:
                ship.update(dt, self.win_size)
            elif not ship.missile.alive:
                self.ships.remove(ship)
        self._check_collisions()
        #if not self.asteroids:
            #self._spawn_new_asteroids()

def run_app(num_asteroids, human_player, hotseat, local_bots, remote_connections, recv_port, send_port):
    pyglet.resource.path = ['../resources']
    pyglet.resource.reindex()

    if remote_connections:
        ai_actions = udp_conns.init_remote_conn(recv_port, send_port)
    elif local_bots:
        ai_actions = local_ai.init_local_ai()
    else:
        ai_actions = None

    screen_width, screen_height = monitor.get_resolution()
    window = GameWindow(
        width=screen_width, height=screen_height,
        num_asteroids=num_asteroids,
        human_player=human_player,
        hotseat=hotseat,
        ai_actions=ai_actions
    )
    window.set_fullscreen(False)
    pyglet.clock.schedule_interval(window.update, 1 / FRAMERATE)
    try:
        pyglet.app.run()
    except KeyboardInterrupt as e:
        hiscore = utils.load_hiscore(SAVEFILE)
        if window.hiscore > hiscore:
            with open(SAVEFILE, 'wb') as f:
                pickle.dump([window.hiscore], f, protocol=2)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--asteroids", dest="num_asteroids", help="The number of asteroids in the game", type=int, default=5)
    parser.add_argument("--hotseat", dest="hotseat", action="store_true", help="The game is started in the hotseat mode", default=False)
    parser.add_argument("--no-human-player", dest="human_player", action="store_false", help="The game is started without a human player", default=True)
    parser.add_argument("--remote-connections", dest="remote_connections", action="store_true", help="The game is started with remote connections", default=False)
    parser.add_argument("--local-bots", dest="local_bots", action="store_true", help="The game is started with local bots disabled", default=False)
    parser.add_argument("--recv-port", dest="recv_port", help="The receiving socket port", type=int, default=5005)
    parser.add_argument("--send-port", dest="send_port", help="The sending socket port", type=int, default=5006)
    args = parser.parse_args()
    run_app(args.num_asteroids, args.human_player, args.hotseat, args.local_bots, args.remote_connections, args.recv_port, args.send_port)
