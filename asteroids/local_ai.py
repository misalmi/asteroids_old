import time
import socket
import atexit
import actions
import multiprocessing

import custom_local_ai

PLAYER_DATA_LEN = 8
SPRITE_DATA_LEN = 4

def parse_player_data(data_str):
    data_str = data_str.lstrip("player").strip()
    data = data_str.split(",")
    if len(data) != PLAYER_DATA_LEN:
        raise ParseError("Data string of invalid length (player)!")
    alive, x, y, vx, vy, rot, rot_speed, shot = data
    try:
        parsed = {"alive": alive == "True", "x": float(x), "y": float(y), "vx":  
                  float(vx), "vy": float(vy), "rot": float(rot), "rot_speed" : float(rot_speed), "shot": shot == "True"}
    except ValueError:
        raise ParseError("Data string contained data of invalid type (player)!")
    return parsed

def parse_sprite_data(data_str):
    data_str = data_str.lstrip("ships").lstrip("asteroids").lstrip("missiles").strip()
    data = data_str.split()
    parsed_all = []
    for sprite_data in data:
        sprite_data = sprite_data.split(",")
        if len(sprite_data) != SPRITE_DATA_LEN:
            raise ParseError("Data string of invalid length (sprite)!")
        x, y, vx, vy = sprite_data
        try:
            parsed = {"x": float(x), "y": float(y), "vx":  float(vx), "vy": float(vy)}
        except ValueError:
            raise ParseError("Data string contained data of invalid type (sprite)!")
        parsed_all.append(parsed)
    return parsed_all
    
def parse_data(data, player_data, ship_data, missile_data, asteroid_data):
    if data.startswith("player"):
        try:
            player_data = parse_player_data(data)
        except ParseError as e:
            print(e)
    elif data.startswith("ships"):
        try:
            ship_data = parse_sprite_data(data)
        except ParseError as e:
            print(e)
    elif data.startswith("missiles"):
        try:
            missile_data = parse_sprite_data(data)
        except ParseError as e:
            print(e)
    elif data.startswith("asteroids"):
        try:
            asteroid_data = parse_sprite_data(data)
        except ParseError as e:
            print(e)
    return player_data, ship_data, missile_data, asteroid_data

def recv_and_send(recv_queue, send_queue):
    player_data = {"alive" : False, "x": 0, "y": 0, "vx": 0, "vy": 0, "rot": 0, "rot_speed" : 0, "shot": False}
    ship_data = []
    missile_data = []
    asteroid_data = []
    cmds = []
    last_turn = None
    while True:
        cmds = custom_local_ai.decide_next_action(player_data, asteroid_data, cmds)
        for cmd in cmds:
            print(cmd)
            send_queue.put(((-1, -1), cmd))
        time.sleep(0.04)

        messages = []
        for i in range(10):
            try:
                messages = recv_queue.get_nowait()
            except multiprocessing.queues.Empty:
                pass
                
        for msg in messages:
            player_data, ship_data, missile_data, asteroid_data = parse_data(msg, player_data, ship_data, missile_data, asteroid_data)

def cleanup(process):
    process.terminate()
               
def init_local_ai():
    reader_queue = multiprocessing.Queue()
    writer_queue = multiprocessing.Queue()
    ai_listener = multiprocessing.Process(target=recv_and_send, args=(reader_queue, writer_queue))
    ai_listener.start()
    ai_actions = actions.AIActions(writer_queue, reader_queue)
    atexit.register(cleanup, ai_listener)
    return ai_actions

class ParseError(Exception):
    pass

