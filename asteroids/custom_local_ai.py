import random

MYNAME = "Miikka"

def add_command(cmds, cmd):
    cmds.append(cmd)

def add_commands(cmds, new_cmds):
    cmds.extend(new_cmds)

def decide_next_action(player_data, sprite_data, cmds):
    alive = player_data["alive"]
    player_x, player_y = player_data["x"], player_data["y"]
    player_vx, player_vy = player_data["vx"], player_data["vy"]
    rotation = player_data["rot"]
    rotation_speed = player_data["rot_speed"]
    missile_shot = player_data["shot"]

    cmds = []
    if not alive:
        add_command(cmds, "create " + MYNAME)
    add_command(cmds, "drag")
    add_command(cmds, "shoot")
    add_command(cmds, "rotate_right")

    return cmds
