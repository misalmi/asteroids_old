KEYS = ("accelerate", "drag", "rotate_left", "rotate_right", "shoot")
        
class KeyActions(object):
    def __init__(self, player_ship, key_conf, key_states):
        self.ship = player_ship
        self.key_conf = key_conf
        self.key_states = key_states
        self.last_shot = 0
        
    def update(self, dt, images):
        if not self.ship.alive:
            return

        if self.key_states[self.key_conf["accelerate"]]:
            self.ship.accelerate(dt)
        else:
            self.ship.stop_acceleration()
        if self.key_states[self.key_conf["drag"]]:
            self.ship.drag(dt)
        if self.key_states[self.key_conf["rotate_left"]]:
            self.ship.rotate_left(dt)
        if self.key_states[self.key_conf["rotate_right"]]:
            self.ship.rotate_right(dt)
        if self.key_states[self.key_conf["shoot"]]:
            if self.last_shot > 10:
                self.ship.shoot_missile(dt, images.missile_img, images.explosion_img)
                self.last_shot = 0
        self.last_shot += 1

