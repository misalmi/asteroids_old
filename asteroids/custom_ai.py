import sys
import time
import socket
import atexit
import math
import random
import configparser
import argparse

PLAYER_DATA_LEN = 8
SPRITE_DATA_LEN = 4
SHIP_ZONE = 10

def calc_angle(y, x):
    try:
        angle = math.atan(y / x) / math.pi * 180
    except ZeroDivisionError:
        angle = 0
    if angle < 0 and y > 0:
        angle = angle + 180
    elif angle >= 0 and y < 0:
        angle = angle - 180
    elif y == 0 and x < 0:
        angle = 180
    return angle    
            
def calc_angle_diff(alfa, beta):
    angle_diff = abs(alfa - beta)
    if angle_diff > 180:
        angle_diff = 360 - angle_diff
    return angle_diff

def calc_player_angle(rotation):
    angle = ((-math.radians(rotation) - math.pi / 2) / math.pi * 180) % 360
    if angle > 180:
        angle -= 360
    return angle

def calc_distance(p1, p2): 
    """Returns the distance between two points""" 
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def decide_next_action(player_data, sprite_data, cmds, cfg):
    alive = player_data["alive"]
    player_x, player_y = player_data["x"], player_data["y"]
    player_vx, player_vy = player_data["vx"], player_data["vy"]
    rotation = player_data["rot"]
    rotation_speed = player_data["rot_speed"]
    
    missile_shot = player_data["shot"]

    if not alive:
        return ["create " + cfg["MyName"]]

    if missile_shot:
        cmds = ["shoot"]
    else:
        cmds = ["shoot", "shoot"]

    player_angle = calc_player_angle(rotation)
    player_direction = calc_angle(player_vy, player_vx)
    angle_diff = calc_angle_diff(player_angle, player_direction)
    if rotation_speed > 100:
        cmds.extend(["rotate_left"])
    elif rotation_speed < -100:
        cmds.extend(["rotate_right"])
    elif abs(angle_diff) > cfg["MaxOrientation"]:
        if ((player_angle > player_direction and player_angle - player_direction < 180) or 
            (player_angle < player_direction and player_direction - player_angle > 180)):
            cmds.extend(["rotate_right"])
        else:
            cmds.extend(["rotate_left"])
        return cmds

    for sprite in sprite_data:
        sprite_x, sprite_y = sprite["x"], sprite["y"]
        if sprite_x == player_x and sprite_y == player_y:
            continue
        distance = calc_distance((player_x, player_y), (sprite_x, sprite_y))
        sprite_direction = calc_angle((sprite_y - player_y), (sprite_x - player_x))
        angle_diff = calc_angle_diff(player_angle, sprite_direction)
        if distance < cfg["ScanDistance"] and distance > SHIP_ZONE and abs(angle_diff) < cfg["ScanAngle"]:
            cmds.extend(["drag"] * 3)
            return cmds
        if distance < cfg["DangerZone"] and distance > SHIP_ZONE:
            for i in range(int(abs(cfg["DangerAccelerationFactor"]))):
                cmds.append("accelerate")
    if cmds[-1] == "accelerate":
        return cmds
        
    rotate_decision = random.randrange(100)
    rotate = "rotate_left" if random.randrange(2) else "rotate_right"
    if rotate_decision < cfg["ChanceToRandomlyPivot"]:
        cmds.extend([rotate])
    if abs(player_vx) > cfg["MaxSpeed"] or abs(player_vy) > cfg["MaxSpeed"]:
        cmds.append("drag")
    else:
        cmds.append("accelerate")
    return cmds        

def parse_player_data(data_str):
    data_str = data_str.lstrip("player").strip()
    data = data_str.split(",")
    if len(data) != PLAYER_DATA_LEN:
        raise ParseError("Data string of invalid length (player)!")
    alive, x, y, vx, vy, rot, rot_speed, shot = data
    try:
        parsed = {"alive": alive == "True", "x": float(x), "y": float(y), "vx":  
                  float(vx), "vy": float(vy), "rot": float(rot), "rot_speed" : float(rot_speed), 
                  "shot": shot == "True"}
    except ValueError:
        raise ParseError("Data string contained data of invalid type (player)!")
    return parsed

def parse_sprite_data(data_str):
    data_str = data_str.lstrip("ships").lstrip("asteroids").lstrip("missiles").strip()
    data = data_str.split()
    parsed_all = []
    for sprite_data in data:
        sprite_data = sprite_data.split(",")
        if len(sprite_data) != SPRITE_DATA_LEN:
            raise ParseError("Data string of invalid length (sprite)!")
        x, y, vx, vy = sprite_data
        try:
            parsed = {"x": float(x), "y": float(y), "vx":  float(vx), "vy": float(vy)}
        except ValueError:
            raise ParseError("Data string contained data of invalid type (sprite)!")
        parsed_all.append(parsed)
    return parsed_all
    
def parse_data(data, player_data, ship_data, missile_data, asteroid_data):
    decoded = data.decode()
    if decoded.startswith("player"):
        try:
            player_data = parse_player_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("ships"):
        try:
            ship_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("missiles"):
        try:
            missile_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("asteroids"):
        try:
            asteroid_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    return player_data, ship_data, missile_data, asteroid_data

def recv_and_send(recv_sock, send_sock, send_addr, recv_port, send_port, cfg):
    recv_sock.setblocking(0)
    send_sock.setblocking(0)
    recv_sock.bind(("", recv_port))
    player_data = {"alive" : False, "x": 0, "y": 0, "vx": 0, "vy": 0, "rot": 0, "rot_speed" : 0, "shot": False}
    ship_data = []
    missile_data = []
    asteroid_data = []
    cmds = []
    last_turn = None
    while True:
        if not cmds:
            cmds = decide_next_action(player_data, ship_data + asteroid_data, cmds, cfg)
        try:
            send_sock.sendto(cmds.pop(0).encode(), (send_addr, send_port))
        except socket.error:
            pass
        time.sleep(0.04)
        for i in range(10):
            try:
                data, addr = recv_sock.recvfrom(1024)
            except socket.error:
                pass
            else:
                player_data, ship_data, missile_data, asteroid_data = parse_data(data, player_data, ship_data, missile_data, asteroid_data)

def cleanup(recv_sock, send_sock):
    print("Bye!")
    recv_sock.close()
    send_sock.close()

def parse_config(cfg):
    parsed = {}
    expected_keys = ("MyName", "MaxSpeed", "MaxOrientation", "DangerZone", "DangerAccelerationFactor", "ScanDistance",
                     "ScanAngle", "ChanceToRandomlySpin","ChanceToRandomlyPivot",)
    for k in expected_keys:
        if k not in cfg:
            raise ParseError("Expected key {} not in config!".format(k))
        else:
            if k == "MyName":
                parsed[k] = cfg[k]
                continue
            try:
                parsed[k] = float(cfg[k])
            except ValueError:
                raise ParseError("Failed to parse value of key {}".format(k))
    return parsed
    
class ParseError(Exception):
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--default-config", dest="default_config", action="store_true", help="The game is loaded with default config", default=False)
    parser.add_argument("--host", dest="host", type=str, help="The host to which to connect the AI", default="127.0.0.1")
    parser.add_argument("--recv-port", dest="recv_port", type=int, help="The port from which to listen the server messages", default=5006)
    parser.add_argument("--send-port", dest="send_port", type=int, help="The port to which to send the messages to server", default=5005)
    args = parser.parse_args()
    config = configparser.ConfigParser()
    config.read("ai_config.ini")
    if args.default_config:
        if "DEFAULT" not in config:
            sys.exit("Failed to read default config!")
        else:
            cfg = config["DEFAULT"]
    else:
        if "CUSTOM" not in config:
            sys.exit("Failed to read custom config!")
        else:
            cfg = config["CUSTOM"]

    try:
        cfg = parse_config(cfg)
    except ParseError as e:
        sys.exit(e)
    recv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    atexit.register(cleanup, recv_sock, send_sock)
    recv_and_send(recv_sock, send_sock, args.host, args.recv_port, args.send_port, cfg)
