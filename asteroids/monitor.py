import os
import subprocess

DEFAULT_WIDTH = 1920
DEFAULT_HEIGHT = 1080

def get_resolution_unix():
    cmd = ['xrandr']
    cmd2 = ['grep', '*']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
    p.stdout.close()
 
    resolution_string, junk = p2.communicate()
    resolution = resolution_string.split()[0]
    try:
        width, height = resolution.decode().split('x')
        return int(width), int(height)
    except ValueError:
        print("Warning: failed to parse screen resolution!")
        return DEFAULT_WIDTH, DEFAULT_HEIGHT

def get_resolution_windows():
    from win32api import GetSystemMetrics
    return GetSystemMetrics(0), GetSystemMetrics(1)

def get_resolution():
    if os.name == "nt":
        return get_resolution_windows()
    else:
        return get_resolution_unix()
