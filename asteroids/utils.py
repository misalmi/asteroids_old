import math
import random
import pickle

def load_hiscore(filename):
    DEFAULT_SCORE = 0
    try:
        with open(filename, 'rb') as f:
            hiscore = pickle.load(f)[0]
    except IOError as e:
        print("Failed to load high score data: ", e)
        return DEFAULT_SCORE
    except IndexError as e:
        print("High score data was of incorrect format: ", e)
        return DEFAULT_SCORE
    else:
        return hiscore

def center_image(image): 
    """Sets an image's anchor point to its center""" 
    image.anchor_x = image.width / 2 
    image.anchor_y = image.height / 2
    return image

def calc_distance(p1, p2): 
    """Returns the distance between two points""" 
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def search_pos(first_pos, sprites, win_size):
    min_dist = 400
    width, height = win_size
    pos = first_pos
    i = 0
    while True:
        if i > 1000:
            min_dist -= 10
        for sprite in sprites:
            x, y = pos
            x_not_acceptable = x < min_dist or x > width - min_dist
            y_not_acceptable = y < min_dist or y > height - min_dist
            if calc_distance(sprite.position, pos) < min_dist or x_not_acceptable or y_not_acceptable:
                pos = random.randint(0, width), random.randint(0, height)
                break
        else:
            break
        i += 1
    return pos

class Images(object):
    def __init__(self, ship_img, flame_img, missile_img, asteroid_img, explosion_img):
        self.ship_img = ship_img
        self.flame_img = flame_img
        self.missile_img = missile_img
        self.asteroid_img = asteroid_img
        self.explosion_img = explosion_img
