import time
import socket
import atexit
import math
import random

PLAYER_DATA_LEN = 7
SPRITE_DATA_LEN = 4

MYNAME = "miikka"
MAX_SPEED = 150.0
SPRITE_FAR = 400.0
SPRITE_NEAR = 275.0
SHIP_ITSELF = 10
MAX_ANGLE_DIFF = 50
SCAN_ANGLE = 50
SPIN = 1
ROTATE = 10

def calc_angle(y, x):
    try:
        angle = math.atan(y / x) / math.pi * 180
    except ZeroDivisionError:
        angle = 0
    if angle < 0 and y > 0:
        angle = angle + 180
    elif angle >= 0 and y < 0:
        angle = angle - 180
    elif y == 0 and x < 0:
        angle = 180
    return angle    
            
def calc_angle_diff(alfa, beta):
    angle_diff = abs(alfa - beta)
    if angle_diff > 180:
        angle_diff = 360 - angle_diff
    return angle_diff

def calc_player_angle(rotation):
    angle = ((-math.radians(rotation) - math.pi / 2) / math.pi * 180) % 360
    if angle > 180:
        angle -= 360
    return angle

def calc_distance(p1, p2): 
    """Returns the distance between two points""" 
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def decide_next_action(player_data, sprite_data, cmds):
    alive = player_data["alive"]
    player_x, player_y = player_data["x"], player_data["y"]
    player_vx, player_vy = player_data["vx"], player_data["vy"]
    rotation = player_data["rot"]
    missile_shot = player_data["shot"]

    if not alive:
        return ["create " + MYNAME]

    if missile_shot:
        cmds = ["shoot"]
    else:
        cmds = ["shoot", "shoot"]

    player_angle = calc_player_angle(rotation)
    player_direction = calc_angle(player_vy, player_vx)
    angle_diff = calc_angle_diff(player_angle, player_direction)
    if abs(angle_diff) > MAX_ANGLE_DIFF:
        if ((player_angle > player_direction and player_angle - player_direction < 180) or 
            (player_angle < player_direction and player_direction - player_angle > 180)):
            cmds.extend(["rotate_right"] * 2)
        else:
            cmds.extend(["rotate_left"] * 2)
        return cmds

    for sprite in sprite_data:
        sprite_x, sprite_y = sprite["x"], sprite["y"]
        if sprite_x == player_x and sprite_y == player_y:
            continue
        distance = calc_distance((player_x, player_y), (sprite_x, sprite_y))
        sprite_direction = calc_angle((sprite_y - player_y), (sprite_x - player_x))
        angle_diff = calc_angle_diff(player_angle, sprite_direction)
        if distance < SPRITE_FAR and distance > SHIP_ITSELF and abs(angle_diff) < SCAN_ANGLE:
            cmds.extend(["drag"] * 3)
            return cmds
        if distance < SPRITE_NEAR and distance > SHIP_ITSELF:
            cmds.append("accelerate")
    if cmds[-1] == "accelerate":
        return cmds
        
    rotate_decision = random.randrange(100)
    rotate = "rotate_left" if random.randrange(2) else "rotate_right"
    if rotate_decision == SPIN:
        cmds.extend(["drag"] * 3)
        cmds.extend([rotate] * 5)
        cmds.extend(["accelerate"] * 3)
        return cmds
    elif rotate_decision < ROTATE:
        cmds.extend([rotate] * 3)
    if abs(player_vx) > MAX_SPEED or abs(player_vy) > MAX_SPEED:
        cmds.append("drag")
    else:
        cmds.append("accelerate")
    return cmds        

def parse_player_data(data_str):
    data_str = data_str.lstrip("player").strip()
    data = data_str.split(",")
    if len(data) != PLAYER_DATA_LEN:
        raise ParseError("Data string of invalid length (player)!")
    alive, x, y, vx, vy, rot, shot = data
    try:
        parsed = {"alive": alive == "True", "x": float(x), "y": float(y), "vx":  
                  float(vx), "vy": float(vy), "rot": float(rot), "shot": shot == "True"}
    except ValueError:
        raise ParseError("Data string contained data of invalid type (player)!")
    return parsed

def parse_sprite_data(data_str):
    data_str = data_str.lstrip("ships").lstrip("asteroids").lstrip("missiles").strip()
    data = data_str.split()
    parsed_all = []
    for sprite_data in data:
        sprite_data = sprite_data.split(",")
        if len(sprite_data) != SPRITE_DATA_LEN:
            raise ParseError("Data string of invalid length (sprite)!")
        x, y, vx, vy = sprite_data
        try:
            parsed = {"x": float(x), "y": float(y), "vx":  float(vx), "vy": float(vy)}
        except ValueError:
            raise ParseError("Data string contained data of invalid type (sprite)!")
        parsed_all.append(parsed)
    return parsed_all
    
def parse_data(data, player_data, ship_data, missile_data, asteroid_data):
    decoded = data.decode()
    if decoded.startswith("player"):
        try:
            player_data = parse_player_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("ships"):
        try:
            ship_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("missiles"):
        try:
            missile_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    elif decoded.startswith("asteroids"):
        try:
            asteroid_data = parse_sprite_data(decoded)
        except ParseError as e:
            print(e)
    return player_data, ship_data, missile_data, asteroid_data

def recv_and_send(recv_sock, send_sock, send_addr, recv_port, send_port):
    recv_sock.setblocking(0)
    send_sock.setblocking(0)
    recv_sock.bind(("", recv_port))
    player_data = {"alive" : False, "x": 0, "y": 0, "vx": 0, "vy": 0, "rot": 0, "shot": False}
    ship_data = []
    missile_data = []
    asteroid_data = []
    cmds = []
    last_turn = None
    while True:
        if not cmds:
            cmds = decide_next_action(player_data, ship_data + asteroid_data, cmds)
        try:
            send_sock.sendto(cmds.pop(0).encode(), (send_addr, send_port))
        except socket.error:
            pass
        time.sleep(0.04)
        for i in range(10):
            try:
                data, addr = recv_sock.recvfrom(1024)
                print(data)
            except socket.error:
                pass
            else:
                player_data, ship_data, missile_data, asteroid_data = parse_data(data, player_data, ship_data, missile_data, asteroid_data)

def cleanup(recv_sock, send_sock):
    print("Bye!")
    recv_sock.close()
    send_sock.close()

class ParseError(Exception):
    pass

if __name__ == "__main__":
    recv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    atexit.register(cleanup, recv_sock, send_sock)
    recv_and_send(recv_sock, send_sock, "127.0.0.1", 5006, 5005)
