import socket
import multiprocessing
import atexit

import actions

def cleanup(recv_sock, send_sock, process):
    print("Bye!")
    recv_sock.close()
    send_sock.close()
    process.terminate()

def recv_and_send(recv_sock, send_sock, recv_port, send_port, reader_queue, writer_queue):
    recv_sock.setblocking(0)
    send_sock.setblocking(0)
    recv_sock.bind(("", recv_port))
    while True:
        try:
            data, addr = recv_sock.recvfrom(1024)
            reader_queue.put((addr, data.decode()))
        except socket.error:
            pass
        try:
            data, addr = writer_queue.get_nowait()
        except multiprocessing.queues.Empty:
            pass
        else:
            send_sock.sendto(data.encode(), (addr, send_port))
    
def init_remote_conn(recv_port, send_port):
    reader_queue = multiprocessing.Queue()
    writer_queue = multiprocessing.Queue()
    recv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    remote_listener = multiprocessing.Process(target=recv_and_send, args=(recv_sock, send_sock, recv_port, send_port, reader_queue, writer_queue))
    remote_listener.start()
    remote_actions = actions.AIActions(reader_queue, writer_queue)
    atexit.register(cleanup, recv_sock, send_sock, remote_listener)
    return remote_actions
