import random
import math
from collections import namedtuple

import pyglet

import utils

ASTEROID_ROTATE_FACTOR = 100.0

def create_asteroid(img, scale, v_factor, batch, pos):
    x, y = pos
    vx_factor, vy_factor = v_factor
    asteroid = PhysicsSprite(img=img, x=x, y=y, batch=batch)
    asteroid.group = pyglet.graphics.OrderedGroup(2)
    asteroid.scale = scale
    asteroid.velocity_x = (random.random() * 2 - 1) * vx_factor
    asteroid.velocity_y = (random.random() * 2 - 1) * vy_factor
    asteroid.rotation = random.randrange(0, 360)
    asteroid.rotate_speed = (random.random() * 2 - 1) * ASTEROID_ROTATE_FACTOR
    return asteroid

def create_asteroids(n, img, scale, v_factor, batch, pos):
    asteroids = []
    for i in range(n):
        asteroid = create_asteroid(img, scale, v_factor, batch, pos)
        asteroids.append(asteroid)
    return asteroids

def create_asteroids_randomly(n, img, scale, v_factor, batch, win_size):
    width, height = win_size
    asteroids = []
    for i in range(n):
        pos = random.randint(0, width), random.randint(0, height)
        asteroid = create_asteroid(img, scale, v_factor, batch, pos)
        asteroids.append(asteroid)
    return asteroids

def create_ship(name, images, pos, batch, velocity=None):
    x, y = pos
    ship = Ship(name, images.ship_img, images.flame_img, images.missile_img, x, y, batch)
    if velocity is not None:
        v_x, v_y = velocity
        ship.set_velocity(v_x, v_y)
    return ship

def collided(sprite1, sprite2, safe_area=0):
    img1 = sprite1.image
    img2 = sprite2.image
    dimension1 = (img1.width + img1.height) / 2
    dimension2 = (img2.width + img2.height) / 2
    collision_distance = dimension1 / 2 + dimension2 / 2 - safe_area
    actual_distance = utils.calc_distance(sprite1.position, sprite2.position)
    return actual_distance <= collision_distance

class GameSprite(pyglet.sprite.Sprite):
    def __init__(self, *args, **kwargs):
        super(GameSprite, self).__init__(*args, **kwargs)
        self.alive = True
        self.group = pyglet.graphics.OrderedGroup(0)

    def destroy(self, *args, **kwargs):
        self.delete()
        self.alive = False

    def explode(self, img, *args, **kwargs):
        self.destroy()
        explosion = GameSprite(img=img, x=self.x, y=self.y, batch=self.batch)
        pyglet.clock.schedule_once(explosion.destroy, 1.0)

class PhysicsSprite(GameSprite):
    rotate_speed = 0.0

    def __init__(self, *args, **kwargs):
        super(PhysicsSprite, self).__init__(*args, **kwargs)
        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.rotate_speed = PhysicsSprite.rotate_speed

    def check_bounds(self, win_size):
        win_width, win_height = win_size
        min_x = -self.image.width / 2
        min_y = -self.image.height / 2
        max_x = win_width + self.image.width / 2
        max_y = win_height + self.image.height / 2
        if self.x < min_x:
            self.x = max_x
        elif self.x > max_x:
            self.x = min_x
        if self.y < min_y:
            self.y = max_y
        elif self.y > max_y:
            self.y = min_y

    def update(self, dt, win_size):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt
        self.check_bounds(win_size)
        self.rotation += self.rotate_speed * dt

class MissileSprite(PhysicsSprite):
    speed = 1000.0

    def __init__(self, *args, **kwargs):
        super(MissileSprite, self).__init__(*args, **kwargs)
        self.visible = False
        self.speed = MissileSprite.speed
        self.group = pyglet.graphics.OrderedGroup(0)

    def is_shot(self):
        return self.visible and self.alive

    def shoot(self, ship_x, ship_y, ship_vx, ship_vy, ship_rotation, ship_radius):
        angle = -math.radians(ship_rotation) - math.pi / 2
        self.x = ship_x + math.cos(angle) * ship_radius
        self.y = ship_y + math.sin(angle) * ship_radius
        self.velocity_x = ship_vx + math.cos(angle) * self.speed
        self.velocity_y = ship_vy + math.sin(angle) * self.speed
        self.rotation = ship_rotation
        self.visible = True

    def collided_with(self, sprite):
        MISSILE_SAFE_AREA = 30
        return self.is_shot() and collided(sprite, self, MISSILE_SAFE_AREA)

class ShipSprite(PhysicsSprite):
    velocity_x = 100.0
    velocity_y = 100.0
    thrust = 450.0
    angular_thrust = 8.0

    def __init__(self, *args, **kwargs):
        super(ShipSprite, self).__init__(*args, **kwargs)
        self.velocity_x, self.velocity_y = ShipSprite.velocity_x, ShipSprite.velocity_y
        self.thrust = ShipSprite.thrust
        self.angular_thrust = ShipSprite.angular_thrust

    def _calc_force(self, dt):
        angle = -math.radians(self.rotation) - math.pi / 2
        force_x = math.cos(angle) * self.thrust * dt
        force_y = math.sin(angle) * self.thrust * dt
        return force_x, force_y

    def accelerate(self, dt):
        force_x, force_y = self._calc_force(dt)
        self.velocity_x += force_x
        self.velocity_y += force_y

    def drag(self, dt):
        force_x, force_y = self._calc_force(dt)
        self.velocity_x -= force_x
        self.velocity_y -= force_y

    def rotate_left(self, dt):
        self.rotate_speed -= self.angular_thrust

    def rotate_right(self, dt):
        self.rotate_speed += self.angular_thrust

class Ship(object):
    NAME_LABEL_X_OFFSET = 50
    NAME_LABEL_Y_OFFSET = 75

    default_pos = 300, 400

    def __init__(self, name, ship_img, flame_img, missile_img, x, y, batch):
        self.ship_sprite = ShipSprite(img=ship_img, x=x, y=y, batch=batch)
        self.ship_sprite.group = pyglet.graphics.OrderedGroup(1)
        self.flame = ShipSprite(img=flame_img, x=x, y=y, batch=batch)
        self.flame.group = pyglet.graphics.OrderedGroup(0)
        self.flame.scale = 0.20
        self.flame.visible = False
        self.missile = MissileSprite(img=missile_img, x=x, y=y, batch=batch)
        self.name_label = pyglet.text.Label(name, font_size=10,
                                            x=x + self.NAME_LABEL_X_OFFSET,
                                            y=y + self.NAME_LABEL_Y_OFFSET,
                                            anchor_x="center", anchor_y="center",
                                            batch=batch, group=pyglet.graphics.OrderedGroup(2))
        self.alive = True
        self.score = 0

    def get_ship_img(self):
        return self.ship_sprite.image

    def get_position(self):
        return self.ship_sprite.position

    def set_velocity(self, v_x, v_y):
        self.ship_sprite.velocity_x, self.ship_sprite.velocity_y = v_x, v_y
        self.flame.velocity_x, self.flame.velocity_y = v_x, v_y

    def accelerate(self, dt):
        self.ship_sprite.accelerate(dt)
        self.flame.accelerate(dt)
        self.flame.visible = True

    def stop_acceleration(self):
        self.flame.visible = False

    def drag(self, dt):
        self.ship_sprite.drag(dt)
        self.flame.drag(dt)

    def rotate_left(self, dt):
        self.ship_sprite.rotate_left(dt)
        self.flame.rotate_left(dt)

    def rotate_right(self, dt):
        self.ship_sprite.rotate_right(dt)
        self.flame.rotate_right(dt)

    def shoot_missile(self, dt, missile_img, explosion_img):
        if self.missile.is_shot() and utils.calc_distance(self.ship_sprite.position, self.missile.position) > 100:
            self.missile.explode(explosion_img)
            self.missile = MissileSprite(img=missile_img, x=self.ship_sprite.x, y=self.ship_sprite.y,
                                         batch=self.ship_sprite.batch)
            return
        ship_radius = self.ship_sprite.image.width / 2
        self.missile.shoot(self.ship_sprite.x, self.ship_sprite.y, self.ship_sprite.velocity_x,
                           self.ship_sprite.velocity_y, self.ship_sprite.rotation, ship_radius)

    def hide_missile(self):
        self.missile.visible = False
        if not self.alive:
            self.missile.destroy()

    def collided_with(self, sprite):
        SHIP_SAFE_AREA = 100
        MAX_SPRITE_SCALE = 1.50
        return collided(self.ship_sprite, sprite, SHIP_SAFE_AREA * (MAX_SPRITE_SCALE - sprite.scale))

    def update(self, dt, win_size):
        self.ship_sprite.update(dt, win_size)
        self.flame.update(dt, win_size)
        self.flame.x = self.ship_sprite.x
        self.flame.y = self.ship_sprite.y
        self.name_label.begin_update()
        self.name_label.x = self.ship_sprite.x + self.NAME_LABEL_X_OFFSET
        self.name_label.y = self.ship_sprite.y + self.NAME_LABEL_Y_OFFSET
        self.name_label.end_update()

    def destroy(self, explosion_img):
        self.ship_sprite.explode(explosion_img)
        self.flame.destroy()
        self.name_label.delete()
        self.alive = False
        if not self.missile.visible:
            self.missile.destroy()
